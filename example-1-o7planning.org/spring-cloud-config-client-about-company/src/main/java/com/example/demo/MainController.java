/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.demo;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 16, 2019
 */
@RefreshScope
@RestController
public class MainController {

  @Autowired
  DataSource dataSource;

  @Autowired
  ContactRepository contactRepository;

  @GetMapping("data-source")
  public String getDataSource() {
    return dataSource.toString();
  }

  @GetMapping("contact") 
  public List<Contact> getAllContacts() {
    return contactRepository.findAll();
  }

}
