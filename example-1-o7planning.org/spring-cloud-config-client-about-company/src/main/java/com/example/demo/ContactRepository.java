/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 16, 2019
 */
public interface ContactRepository extends JpaRepository<Contact, Long> {

}
