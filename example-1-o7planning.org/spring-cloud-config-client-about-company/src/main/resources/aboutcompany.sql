create database aboutcompany;
use aboutcompany;

CREATE TABLE `contact` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,  
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,  
  `department` varchar(255) NOT NULL,  
  PRIMARY KEY (`id`)
) ;

insert into contact(`id`, `name`, `phone`, `email`, `department`) values(1, 'dovankhoi', '0976896988', 'khoivando@gmail.com', 'IT');

select * from contact;