/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.demo;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 21, 2019
 */
public class Employee {
  private String name;
  private int id;

  public Employee(int id, String name) {
    super();
    this.id=id;
    this.name=name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "Employee [name=" + name + ", id=" + id + "]";
  }
}
