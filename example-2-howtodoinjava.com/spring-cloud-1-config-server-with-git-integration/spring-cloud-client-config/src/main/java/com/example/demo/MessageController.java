/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 17, 2019
 */
@RestController
@RefreshScope
public class MessageController {
  
  @Value("${msg:Hello world - Config Server is not working..... pelase check}")
  private String msg;
  
  @GetMapping("msg")
  public String getMessage() {
    return msg;
  }
  
}
