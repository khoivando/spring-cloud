/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.demo;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 18, 2019
 */
public class Student {
  private String name;
  private String className;


  public Student(String name, String className) {
    super();
    this.name = name;
    this.className = className;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }
}
