/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.demo;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 18, 2019
 */
@Service
public class StudentService {

  @Autowired  
  RestTemplate restTemplate;

  public String callStudentServiceAndGetData(String schoolname) {
    System.out.println("Consul Demo - Getting School details for " + schoolname);
    String url = "http://student-service/getStudentDetailsForSchool/{schoolname}";
    String response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {}, schoolname).getBody();
    System.out.println("Response Received as " + response + " -  " + new Date());
    return "School Name -  " + schoolname + " :::  Student Details " + response + " -  " + new Date();
  }

  @Bean
  @LoadBalanced
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

}
