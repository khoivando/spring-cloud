/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 18, 2019
 */
@RestController
public class SchoolController {

  @Autowired
  StudentService studentService;

  @RequestMapping(value = "/getSchoolDetails/{schoolname}", method = RequestMethod.GET)
  public String getStudents(@PathVariable String schoolname) {
    System.out.println("Going to call student service to get data!");
    return studentService.callStudentServiceAndGetData(schoolname);
  }

}
