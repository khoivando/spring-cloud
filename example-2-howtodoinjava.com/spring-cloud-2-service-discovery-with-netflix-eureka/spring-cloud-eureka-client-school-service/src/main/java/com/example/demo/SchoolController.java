/***************************************************************************
 * Copyright 2018 by KION - All rights reserved.                *    
 **************************************************************************/
package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *  Author : khoidv
 *  Email  : khoivando@gmail.com
 *  Jan 17, 2019
 */
@RestController
public class SchoolController {

  @Autowired
  RestTemplate restTemplate;

  @GetMapping("/getSchoolDetails/{schoolname}")
  public String getSchool(@PathVariable String schoolname) {
    System.out.println("Getting School details for " + schoolname);
    String url = "http://localhost:8098/getStudentDetailsForSchool/{schoolname}";
    String response = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {}, schoolname).getBody();
    System.out.println("Response Received as " + response);
    return "School Name -  " + schoolname + " \n Student Details " + response;
  }

}
