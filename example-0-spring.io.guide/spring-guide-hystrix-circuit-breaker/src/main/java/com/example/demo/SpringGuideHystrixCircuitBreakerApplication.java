package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class SpringGuideHystrixCircuitBreakerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringGuideHystrixCircuitBreakerApplication.class, args);
	}
	
	@GetMapping("recommended") 
	public String readingList(){
    return "Spring in Action (Manning), Cloud Native Java (O'Reilly), Learning Spring Boot (Packt)";
  }

}

