package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

@RestController
@SpringBootApplication
@RibbonClient(name = "say-hello", configuration = SayHelloConfiguration.class)
public class SpringGuideNetflixRibbonUserServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringGuideNetflixRibbonUserServiceApplication.class, args);
  }

  @Bean
  @LoadBalanced
  RestTemplate restTemplate(){
    return new RestTemplate();  
  }

  @Autowired
  RestTemplate restTemplate;
  
  @GetMapping("he")
  public String he() {
    return "he";
  }

  @GetMapping("hi")
  public String hi() {
    System.out.println("call hi hi");
    String greeting = this.restTemplate.getForObject("http://say-hello/greeting", String.class);
    return String.format("%s, %s!", greeting, "khoidv");
  }

}

