package com.example.springcloudcontractconsumber;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author KhoiDV
 * @date 2/1/2019
 */
@RestController
public class BasicMathController {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/calculate")
    public String checkNumber(@RequestParam(name = "number", required = true) Integer number) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", MediaType.APPLICATION_JSON.toString());

        ResponseEntity<String> responseEntity = restTemplate.exchange(
          "http://localhost:8090/validate/prime-number?number=" + number,
          HttpMethod.GET,
          new HttpEntity<>(httpHeaders),
          String.class
        );

        return responseEntity.getBody();
    }

}
