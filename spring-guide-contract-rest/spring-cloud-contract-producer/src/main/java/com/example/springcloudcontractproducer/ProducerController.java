package com.example.springcloudcontractproducer;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author KhoiDV
 * @date 2/1/2019
 */
@RestController
public class ProducerController {

    @GetMapping("/validate/prime-number")
    //@GetMapping("/validate/prime-number")
    public String isNumberPrime(@RequestParam(name = "number", required = true) Integer number) {
        return number % 2 == 0 ? "Even":"Odd";
    }

}
