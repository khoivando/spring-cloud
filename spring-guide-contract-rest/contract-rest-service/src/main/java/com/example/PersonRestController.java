package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author KhoiDV
 * @date 1/30/2019
 */
@RestController
@RequestMapping("person")
public class PersonRestController {

    @Autowired
    private PersonService service;

    @GetMapping("{id}")
    public Person findByPersonId(@PathVariable Long id) {
        return service.findPersonById(id);
    }

}
