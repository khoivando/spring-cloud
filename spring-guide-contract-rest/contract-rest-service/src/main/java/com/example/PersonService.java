package com.example;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author KhoiDV
 * @date 1/30/2019
 */
@Service
public class PersonService {
    private final Map<Long, Person> personMap;

    public PersonService() {
        personMap = new HashMap<>(3);
        personMap.put(1l, new Person(1l, "Richar", "Gere"));
        personMap.put(2l, new Person(2l, "Emma", "Choplin"));
        personMap.put(3l, new Person(3l, "Anna", "Carolina"));
    }

    public Person findPersonById(Long id) {
        return personMap.get(id);
    }
}
