package com.example;

/**
 * @author KhoiDV
 * @date 1/30/2019
 */
public class Person {
    private Long id;
    private String name;
    private String surename;

    public Person(Long id, String name, String surename) {
        this.id = id;
        this.name = name;
        this.surename = surename;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurename() {
        return surename;
    }

    public void setSurename(String surename) {
        this.surename = surename;
    }
}
