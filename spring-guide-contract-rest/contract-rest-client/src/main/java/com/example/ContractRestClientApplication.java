package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author KhoiDV
 * @date 1/30/2019
 */
@SpringBootApplication
public class ContractRestClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(ContractRestClientApplication.class, args);
    }
}
