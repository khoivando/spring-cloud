package com.example;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author KhoiDV
 * @date 1/30/2019
 */
@RestController
@RequestMapping("message")
public class MessageRestController {

    private final RestTemplate restTemplate;

    MessageRestController(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }

    @GetMapping("{personId}")
    public String getMessage(@PathVariable Long personId) {
        Person person = restTemplate.getForObject("http://localhost:8000/person/{personId}", Person.class, personId);
        return "Hello "+ person.getName();
    }
}
