package com.example.gameapiproducer;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author KhoiDV
 * @date 2/1/2019
 */
@RestController
public class GameController {

    @PostMapping("/game-manager")
    public Message gameManager(@RequestParam(name = "game", required = true) String game,
                               @RequestBody Player player) {
        System.out.println("player: "+ player.toString());
        if (500 < player.getScore()) {
            return new Message("ELIGIBLE");
        }
        return new Message("NOT ELIGIBLE");
    }

}
