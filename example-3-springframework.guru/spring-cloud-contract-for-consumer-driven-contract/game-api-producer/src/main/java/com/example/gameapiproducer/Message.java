package com.example.gameapiproducer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author KhoiDV
 * @date 2/1/2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private String result;
}
