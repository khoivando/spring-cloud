package com.example.gameapiproducer;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;

/**
 * @author KhoiDV
 * @date 2/1/2019
 */
public class GameBaseClass {
    @Before
    public void setup() {
        RestAssuredMockMvc.standaloneSetup(new GameController());
    }
}
