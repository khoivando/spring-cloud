package com.example.gameapiconsumer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author KhoiDV
 * @date 2/1/2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Player {

    private String name;
    private int score;

}
