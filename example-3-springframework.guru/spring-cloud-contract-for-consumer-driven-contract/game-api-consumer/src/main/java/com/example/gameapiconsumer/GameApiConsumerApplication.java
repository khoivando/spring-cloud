package com.example.gameapiconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameApiConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GameApiConsumerApplication.class, args);
    }

}

