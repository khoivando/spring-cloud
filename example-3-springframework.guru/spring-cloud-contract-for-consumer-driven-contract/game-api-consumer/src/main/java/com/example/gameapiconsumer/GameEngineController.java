package com.example.gameapiconsumer;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * @author KhoiDV
 * @date 2/1/2019
 */
@RestController
public class GameEngineController {

    @PostMapping("/player/{game}")
    public ResponseEntity<String> playGame(@PathVariable String game, @RequestBody Player player) {
        ResponseEntity<String> responseEntity = new RestTemplate().exchange(
                RequestEntity.post(URI.create("http://localhost:8090/game-manager?game="+game))
                .contentType(MediaType.APPLICATION_JSON)
                .body(player), String.class);
        return new ResponseEntity<>(responseEntity.getBody(), HttpStatus.OK);
    }

}
