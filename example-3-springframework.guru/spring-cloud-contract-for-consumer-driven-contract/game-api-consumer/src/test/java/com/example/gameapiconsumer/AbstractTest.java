package com.example.gameapiconsumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.springframework.boot.test.json.JacksonTester;

/**
 * @author KhoiDV
 * @date 2/1/2019
 */
public class AbstractTest {

    public JacksonTester<Player> json;

    @Before
    public void setup() {
        ObjectMapper objectMapper = new ObjectMapper();
        JacksonTester.initFields(this, objectMapper);
    }

}
